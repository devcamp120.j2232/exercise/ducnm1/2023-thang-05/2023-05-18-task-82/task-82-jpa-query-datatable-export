$(document).ready(function () {
    initDataTable();
    getCustomerFromDB();
});

function initDataTable() {
    $("#main-table")
        .DataTable({
            columns: [
                {
                    render: function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                    defaultContent: "",
                },
                { data: "id", defaultContent: "" },
                { data: "lastName", defaultContent: "" },
                { data: "firstName", defaultContent: "" },
                { data: "phoneNumber", defaultContent: "" },
                { data: "address" , defaultContent: ""},
                { data: "city" , defaultContent: ""},
                { data: "state" , defaultContent: ""},
                { data: "postalCode", defaultContent: "" },
                { data: "country", defaultContent: "" },
                { data: "salesRepEmployeeNumber", defaultContent: "" },
                { data: "creditLimit", defaultContent: "" },
            ],
            responsive: true,
            lengthChange: false,
            autoWidth: false,
            buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
        })
        .buttons()
        .container()
        .appendTo("#main-table_wrapper .col-md-6:eq(0)");
}

function getCustomerFromDB() {
    fetch("http://localhost:8080/allCustomers")
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            fillDataTable(data);
        })
        .catch((error) => console.error(error));
}

function fillDataTable(data) {
    //draw table
    $("#main-table").DataTable().clear();
    $("#main-table").DataTable().rows.add(data);
    $("#main-table").DataTable().draw();
}
