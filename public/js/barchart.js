$(document).ready(function () {
    loadDataFromDB();
});

function loadDataFromDB() {
    fetch("http://localhost:8080/customers/countBy/USA,France,Spain,Singapore")
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            createBarChart(data);
        })
        .catch((error) => console.error(error));
}

function createBarChart(data) {
    var areaChartData = {
        labels: [],
        datasets: [
            {
                label: "Customer",
                backgroundColor: "rgba(60,141,188,0.9)",
                borderColor: "rgba(60,141,188,0.8)",
                pointRadius: false,
                pointColor: "#3b8bba",
                pointStrokeColor: "rgba(60,141,188,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(60,141,188,1)",
                data: [],
            },
        ],
    };

    data.forEach(element => {
        //add element.countryName to areaChartData.labels
        areaChartData.labels.push(element.countryName);
        //add element.count to areaChartData.datasets[0].data
        areaChartData.datasets[0].data.push(element.count);
    });

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChartData = $.extend(true, {}, areaChartData);
    var temp0 = areaChartData.datasets[0];
    //var temp1 = areaChartData.datasets[1];
    barChartData.datasets[0] = temp0;
    //barChartData.datasets[1] = temp0;

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false,
    };

    new Chart(barChartCanvas, {
        type: "bar",
        data: barChartData,
        options: barChartOptions,
    });
}
