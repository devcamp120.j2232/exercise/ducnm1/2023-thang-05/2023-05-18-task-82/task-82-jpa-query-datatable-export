package com.devcamp.pizza365.interfaces;

public interface ICustomerCount {
    String getCountryName();
    int getCount();
}
