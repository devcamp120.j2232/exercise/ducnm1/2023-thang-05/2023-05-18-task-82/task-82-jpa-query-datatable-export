package com.devcamp.pizza365.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CustomerCountDTO;
import com.devcamp.pizza365.interfaces.ICustomerCount;
import com.devcamp.pizza365.repository.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public List<ICustomerCount> getCustomerCountByCountryName(List<String> countryNames) {
        return customerRepository.getCustomerCountByCountryName(countryNames);
    }
}
