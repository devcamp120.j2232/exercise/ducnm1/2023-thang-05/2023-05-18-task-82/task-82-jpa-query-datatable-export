package com.devcamp.pizza365.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWorkbook {
    private XSSFWorkbook workbook;
    private List<XSSFSheet> worksheets;

    public ExcelWorkbook() {
        this.workbook = new XSSFWorkbook();
        this.worksheets = new ArrayList<XSSFSheet>();
    }

    public XSSFSheet createSheet(String sheetName) {
        XSSFSheet sheet = workbook.createSheet(sheetName);
        worksheets.add(sheet);
        return sheet;
    }

    public List<XSSFSheet> getWorksheets() {
        return this.worksheets;
    }

    public XSSFSheet getWorksheet(String sheetName) {
        for (XSSFSheet sheet : worksheets) {
            if (sheet.getSheetName().equals(sheetName)) {
                return sheet;
            }
        }
        return null; // Sheet with the given name not found
    }

}
