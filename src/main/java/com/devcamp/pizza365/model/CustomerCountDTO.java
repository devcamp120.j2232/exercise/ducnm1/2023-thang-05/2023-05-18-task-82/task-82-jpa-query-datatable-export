package com.devcamp.pizza365.model;

import com.devcamp.pizza365.interfaces.ICustomerCount;

public class CustomerCountDTO implements ICustomerCount {
    private String countryName;
    private int count;

    // public CustomerCountDTO() {
    // }

    public CustomerCountDTO(String countryName, int count) {
        this.countryName = countryName;
        this.count = count;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
