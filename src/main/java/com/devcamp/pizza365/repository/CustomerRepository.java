package com.devcamp.pizza365.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.interfaces.ICustomerCount;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    //qeuery with param

    @Query(value = "Select customers.country as countryName, Count(customers.id) As count From customers Where customers.country In :countryNames Group By customers.country", nativeQuery = true)
    public List<ICustomerCount> getCustomerCountByCountryName(@Param("countryNames") List<String> countryNames);
}
